import bodyParser from 'body-parser';
import cors from 'cors';
import errorhandler from 'errorhandler';
import express from 'express';
import responseTime from 'response-time';

export default function buildApp (name) {
  var app = express();

  app.use(responseTime());
  app.disable('x-powered-by');
  app.use(cors());

  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());

  app.use(errorhandler({ dumpExceptions: true, showStack: true }));

  app.get('/', function (req, res) {
    res.json({ message: name });
  })

  return app;
};
