'use strict';

require('babel/register');

let http = require('http');

let Redbird = require('redbird');

let appBuilder = require('./src/app-maker');


let apps = [];

['alpha', 'beta', 'gamma'].forEach(function (name, index) {
  apps.push(appBuilder(name));
});

let port = 3001;
apps.forEach(function (app, index) {
  let httpServer = http.createServer(app);
  httpServer.listen(port++);
  httpServer.on('listening', function () {
      console.log('Listening on: ' + httpServer.address().port);
  });
});


let proxy = Redbird({ port: 3000 });

proxy.register('localhost:3000', 'localhost:3001');
proxy.register('localhost:3000', 'localhost:3002');
proxy.register('localhost:3000', 'localhost:3003');

proxy.register('localhost:3000/alpha', 'localhost:3001');
proxy.register('localhost:3000/beta', 'localhost:3002');
proxy.register('localhost:3000/gamma', 'localhost:3003');
