# Readbird Test

A small application to test [Redbird](https://github.com/OptimalBits/redbird)
reverse proxying.

By Ben

*Because learning.*


# Install

```bash
 npm install            # Install dependencies
```

# Run

```bash
node serve.js
```

This throws up 4 servers.

* Proxy server `localhost:3000`
* Alpha server `localhost:3001`
* Beta server `localhost:3002`
* Gamma server `localhost:3003`

Alpha, Beta and Gamma can all be accessed directly on their port numbers.

They can also be accessed via the proxy.  Accessing root, `localhost:3001/`,
rotates which server will respond.  Accessing via a named url gives directs to
the appropriate server, `localhost:3001/[alpha|beta|gamma]`.
